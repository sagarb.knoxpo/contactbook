import mongoose from "mongoose";

const contactSchema = new mongoose.Schema({
  firstName: String,
  surName: String,
  phone: { type: String },
  email: { type: String },
  company: String,
  jobTitle: String,
  birthday: Date,
  address: String,
  city: String,
  pincode: Number,
  website: String,
  nickName: String,
  createdAt: { type: Date, default: Date.now }
});

const Contact = mongoose.model("Contact", contactSchema);
export { Contact };
