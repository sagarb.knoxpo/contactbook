import React, { Component, Fragment } from "react";
import { gql } from "apollo-boost";
import { Query } from "react-apollo";
import { Link, RouteComponentProps } from "react-router-dom";

const CONTACTDETAILS_QUERY = gql`
  query GETCONTACTBYIDQuery($_id: String!) {
    getContactById(id: $_id) {
      firstName
      surName
      phone
      email
      company
      jobTitle
      birthday
      address
      city
      pincode
      website
      nickName
    }
  }
`;

export class ContactDetails extends Component<RouteComponentProps> {
  render() {
    //console.log(this.props.match.params);
    const { _id }: any = this.props.match.params;
    return (
      <Fragment>
        <h1 className="display-4 my-3">Contacts</h1>
        {
          <Query query={CONTACTDETAILS_QUERY} variables={{ _id }}>
            {({ loading, error, data }) => {
              if (loading) return <h4>Loading...</h4>;
              if (error) console.log(error);

              const {
                firstName,
                surName,
                email,
                phone,
                company,
                jobTitle,
                nickName,
                address,
                city,
                pincode,
                website
              } = data.getContactById;

              return (
                <div className="container">
                  <div className="card card-body text-left mt-5 mb-5">
                    <h6>
                      Name: {firstName} {surName}
                    </h6>
                    <h6>Email: {email}</h6>
                    <h6>Phone: {phone}</h6>
                    <h6>Company: {company}</h6>
                    <h6>Job Title: {jobTitle}</h6>
                    <h6>Nickname: {nickName}</h6>
                    <h6>Address: {address}</h6>
                    <h6>City: {city}</h6>
                    <h6>Pincode: {pincode}</h6>
                    <h6>website: {website}</h6>
                  </div>
                  <Link to="/" className="btn btn-secondary mt-5">
                    Back
                  </Link>
                </div>
              );
            }}
          </Query>
        }
      </Fragment>
    );
  }
}

export default ContactDetails;
