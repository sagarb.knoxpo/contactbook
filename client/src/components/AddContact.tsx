import React, { Component } from "react";
import { gql } from "apollo-boost";
import { Mutation } from "react-apollo";
import { RouteComponentProps } from "react-router-dom";

const ADDCONTACTQUERY = gql`
  mutation AddContactMutation(
    $firstName: String!
    $surName: String
    $phone: String!
    $email: String
    $company: String
    $jobTitle: String
    $birthday: String
    $address: String
    $city: String
    $pincode: String
    $website: String
    $nickName: String
  ) {
    addContact(
      firstName: $firstName
      surName: $surName
      phone: $phone
      email: $email
      company: $company
      jobTitle: $jobTitle
      birthday: $birthday
      address: $address
      city: $city
      pincode: $pincode
      website: $website
      nickName: $nickName
    ) {
      firstName
      surName
      phone
      email
      company
      jobTitle
      birthday
      address
      city
      pincode
      website
      nickName
    }
  }
`;

interface MyState {
  firstName: string;
  surName: string;
  phone: string;
  email: string;
  company: string;
  jobTitle: string;
  birthday: string;
  address: string;
  city: string;
  pincode: string;
  website: string;
  nickName: string;
}

export class AddContact extends Component<RouteComponentProps, MyState> {
  constructor(props: any) {
    super(props);
    this.state = {
      firstName: "",
      surName: "",
      phone: "",
      email: "",
      company: "",
      jobTitle: "",
      birthday: "",
      address: "",
      city: "",
      pincode: "",
      website: "",
      nickName: ""
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event: any) {
    //const { name, value } = event.target;
    this.setState({
      [event.target.name]: event.target.value
    } as any);
  }

  render() {
    return (
      <div className="container">
        <h1 className="display-4 my-3 text-center">Add Contacts</h1>
        <Mutation mutation={ADDCONTACTQUERY}>
          {(addContact, { data }) => (
            <form
              className="mt-5"
              onSubmit={(event) => {
                addContact({
                  variables: {
                    firstName: this.state.firstName,
                    surName: this.state.surName,
                    phone: this.state.phone,
                    email: this.state.email,
                    company: this.state.company,
                    jobTitle: this.state.jobTitle,
                    birthday: this.state.birthday,
                    address: this.state.address,
                    city: this.state.city,
                    pincode: this.state.pincode,
                    website: this.state.website,
                    nickName: this.state.nickName
                  }
                });
                this.props.history.push("/");
                event.preventDefault();
              }}>
              <div className="form-group">
                <label className="text-left">First Name</label>
                <input
                  type="text"
                  className="form-control"
                  id="firstName"
                  name="firstName"
                  placeholder=""
                  value={this.state.firstName}
                  onChange={this.handleChange}
                />
              </div>
              <div className="form-group">
                <label className="text-left">Last Name</label>
                <input
                  type="text"
                  className="form-control"
                  id="surName"
                  name="surName"
                  placeholder=""
                  value={this.state.surName}
                  onChange={this.handleChange}
                />
              </div>
              <div className="form-group">
                <label className="text-left">Phone</label>
                <input
                  type="text"
                  className="form-control"
                  id="phone"
                  name="phone"
                  placeholder=""
                  value={this.state.phone}
                  onChange={this.handleChange}
                />
              </div>
              <div className="form-group">
                <label className="text-left">Email</label>
                <input
                  type="text"
                  className="form-control"
                  id="email"
                  name="email"
                  placeholder=""
                  value={this.state.email}
                  onChange={this.handleChange}
                />
              </div>
              <div className="form-group">
                <label className="text-left">Company</label>
                <input
                  type="text"
                  className="form-control"
                  id="company"
                  name="company"
                  placeholder=""
                  value={this.state.company}
                  onChange={this.handleChange}
                />
              </div>
              <div className="form-group">
                <label className="text-left">Job Title</label>
                <input
                  type="text"
                  className="form-control"
                  id="jobTitle"
                  name="jobTitle"
                  placeholder=""
                  value={this.state.jobTitle}
                  onChange={this.handleChange}
                />
              </div>
              <div className="form-group">
                <label className="text-left">Birthday</label>
                <input
                  type="text"
                  className="form-control"
                  id="birthday"
                  name="birthday"
                  placeholder=""
                  value={this.state.birthday}
                  onChange={this.handleChange}
                />
              </div>
              <div className="form-group">
                <label className="text-left">Address</label>
                <input
                  type="text"
                  className="form-control"
                  id="address"
                  name="address"
                  placeholder=""
                  value={this.state.address}
                  onChange={this.handleChange}
                />
              </div>
              <div className="form-group">
                <label className="text-left">City</label>
                <input
                  type="text"
                  className="form-control"
                  id="city"
                  name="city"
                  placeholder=""
                  value={this.state.city}
                  onChange={this.handleChange}
                />
              </div>
              <div className="form-group">
                <label className="text-left">Pincode</label>
                <input
                  type="text"
                  className="form-control"
                  id="pincode"
                  name="pincode"
                  placeholder=""
                  value={this.state.pincode}
                  onChange={this.handleChange}
                />
              </div>
              <div className="form-group">
                <label className="text-left">Website</label>
                <input
                  type="text"
                  className="form-control"
                  id="website"
                  name="website"
                  placeholder=""
                  value={this.state.website}
                  onChange={this.handleChange}
                />
              </div>
              <div className="form-group">
                <label className="text-left">Nick name</label>
                <input
                  type="text"
                  className="form-control"
                  id="nickName"
                  name="nickName"
                  placeholder=""
                  value={this.state.nickName}
                  onChange={this.handleChange}
                />
              </div>
              <button type="submit" value="Submit" className="btn btn-primary">
                Submit
              </button>
            </form>
          )}
        </Mutation>
      </div>
    );
  }
}

export default AddContact;
