import { gql } from "apollo-server-express";

const typedefs = gql`
  type Query {
    allContacts: [Contact!]!
    getContactById(id: String!): Contact
  }

  type Mutation {
    addContact(
      firstName: String!
      surName: String
      phone: String!
      email: String
      company: String
      jobTitle: String
      birthday: String
      address: String
      city: String
      pincode: String
      website: String
      nickName: String
    ): Contact
    deleteContact(id: String!): Contact
    updateContact(
      id: String!
      firstName: String!
      surName: String
      phone: String!
      email: String
      company: String
      jobTitle: String
      birthday: String
      address: String
      city: String
      pincode: String
      website: String
      nickName: String
    ): Contact
  }

  type Contact {
    _id: String!
    firstName: String!
    surName: String
    phone: String!
    email: String
    company: String
    jobTitle: String
    birthday: String
    address: String
    city: String
    pincode: String
    website: String
    nickName: String
  }
`;

export default typedefs;
