import express from "express";
import mongoose from "mongoose";
import dotenv from "dotenv";
import server from "./graphql/schema";

dotenv.config({
  path: ".env"
});

const app = express();

const mongoUri = process.env.MONGODB_URI_LOCAL;
(<any>mongoose).Promise = global.Promise;
mongoose.connect(mongoUri, { useNewUrlParser: true }).then(
  () => {
    console.log("\tMongoDB connected successfully.");
    console.log("\tPress CTRL-C to stop\n");
  },
  (err: any) => {
    /** handle initial connection error */
    console.log("MongoDB connection error. Please make sure MongoDB is running. " + err);
  }
);

server.applyMiddleware({ app });

export default app;
