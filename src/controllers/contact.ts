import { Contact } from "../models/Contact";
import { Types } from "mongoose";

const contactController = {
  addContact: (root: any, args: any) => {
    const contact = new Contact(args);
    return contact.save();
  },
  deleteContact: async (root: any, args: any) => {
    return await Contact.findOneAndDelete({ _id: args.id });
  },
  updateContact: async (root: any, args: any) => {
    const tempContact = { ...args };
    delete tempContact.id;
    return await Contact.findOneAndUpdate({ _id: args.id }, { $set: tempContact });
  },
  allContacts: (root: any, args: any) => {
    return Contact.find({});
  },
  getContactById: async (root: any, args: any) => {
    return Contact.findOne({ _id: args.id });
  }
};

export { contactController };
