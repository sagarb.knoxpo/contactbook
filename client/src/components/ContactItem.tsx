import React from "react";
import { Link } from "react-router-dom";
import { gql } from "apollo-boost";
import { Mutation } from "react-apollo";

const DELETECONTACTQUERY = gql`
  mutation deleteContactByIdQuery($_id: String!) {
    deleteContact(id: $_id) {
      _id
    }
  }
`;

export default function ContactItem({ contact: { _id, firstName, email, phone, company } }: any) {
  return (
    <div className="card card-body">
      <Link to={`/details/${_id}`} className="row">
        {/* <div className="col">
     <h6>{_id}</h6>
    </div> */}
        <div className="col text-center">
          <h6>{firstName}</h6>
        </div>
        <div className="col text-center">
          <h6>{email}</h6>
        </div>
        <div className="col text-center">
          <h6>{phone}</h6>
        </div>
        <div className="col text-center">
          <h6>{company}</h6>
        </div>
        <div className="col text-center">
          <Link to={`/updateContact/${_id}`} className="btn btn-primary">
            Update
          </Link>
        </div>
      </Link>
      <div className="col text-center">
        {
          <Mutation mutation={DELETECONTACTQUERY} variables={{ _id }}>
            {(deleteContact, { data }) => (
              <form
                onSubmit={(event) => {
                  deleteContact(), console.log(data);
                  event.preventDefault();
                }}>
                <button className="btn btn-danger" type="submit" value="Submit">
                  Delete
                </button>
              </form>
            )}
          </Mutation>
        }
      </div>
    </div>
  );
}
