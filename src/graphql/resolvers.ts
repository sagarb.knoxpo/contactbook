import { Contact } from "../models/Contact";
import { contactController } from "../controllers/contact";

const resolvers = {
  Mutation: {
    addContact(root: any, args: any) {
      return contactController.addContact(root, args);
    },
    deleteContact(root: any, args: any) {
      return contactController.deleteContact(root, args);
    },
    updateContact(root: any, args: any) {
      return contactController.updateContact(root, args);
    }
  },
  Query: {
    allContacts(root: any, args: any) {
      return contactController.allContacts(root, args);
    },
    getContactById(root: any, args: any) {
      return contactController.getContactById(root, args);
    }
  }
};

export default resolvers;
