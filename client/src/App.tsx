import React, { Component } from "react";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Contacts from "./components/Contacts";
import ContactDetails from "./components/ContactDetails";
import "./App.css";
import AddContact from "./components/AddContact";
import UpdateContact from "./components/UpdateContact";
//import DeleteItem from "./components/DeleteItem";

const client = new ApolloClient({
  uri: "http://localhost:2611/graphql"
});

class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <Router>
          <div className="App">
            <Route exact path="/" component={Contacts} />
            <Route exact path="/details/:_id" component={ContactDetails} />
            <Route exact path="/addContact" component={AddContact} />
            <Route exact path="/updateContact/:_id" component={UpdateContact} />
            {/* <Route exact path="/deleteContact/:_id" component={DeleteItem} /> */}
          </div>
        </Router>
      </ApolloProvider>
    );
  }
}

export default App;
