import React, { Component, Fragment } from "react";
import { gql } from "apollo-boost";
import { Query } from "react-apollo";
import ContactItem from "./ContactItem";

const CONTACTS_QUERY = gql`
  query AllContactsQuery {
    allContacts {
      _id
      firstName
      email
      phone
      company
    }
  }
`;

export class Contacts extends Component {
  render() {
    return (
      <Fragment>
        <h1 className="display-4 my-3 text-center">Contacts</h1>
        <Query query={CONTACTS_QUERY}>
          {({ loading, error, data }) => {
            if (loading) return <h4>Loading...</h4>;
            if (error) console.log(error);
            return (
              <Fragment>
                <div className="card card-body text-white bg-dark">
                  <div className="row">
                    <div className="col text-center">
                      <h6>Name</h6>
                    </div>
                    <div className="col text-center">
                      <h6>Email</h6>
                    </div>
                    <div className="col text-center">
                      <h6>Phone</h6>
                    </div>
                    <div className="col text-center">
                      <h6>Company</h6>
                    </div>
                    <div className="col text-center">
                      <h6>Update</h6>
                    </div>
                    <div className="col text-center">
                      <h6>Delete</h6>
                    </div>
                  </div>
                </div>
                {data.allContacts.map((person: any) => (
                  <ContactItem key={person["_id"]} contact={person} />
                ))}
              </Fragment>
            );
          }}
        </Query>
      </Fragment>
    );
  }
}

export default Contacts;
