import React from "react";
import { gql } from "apollo-boost";
import { Mutation } from "react-apollo";

const DELETECONTACTQUERY = gql`
  mutation deleteContactByIdQuery($_id: String!) {
    deleteContact(id: $_id) {
      _id
    }
  }
`;

export default function DeleteItem() {
  return <div />;
}
